import SparkMD5 from 'spark-md5'

onmessage = function(event) {
  const book = event.data.book

  console.log('worker：嗯嗯，我要开始计算《' + book + '》了')
  const hash = SparkMD5.hash(book)

  console.log('worker：我计算好啦')
  postMessage(hash)
}
