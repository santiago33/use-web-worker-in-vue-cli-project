# 在 Vue CLI 项目中使用 Web Worker

在 Vue CLI 项目中使用 Web Worker：

```JavaScript
const worker1 = new Worker('./workers/my.worker1.js')
const worker2 = new Worker('./workers/my.worker2.js', { type: 'classic' })
const worker3 = new Worker('./workers/my.worker3.js', { type: 'module' })
```

上述代码使用了三种方式来实例化 Web Worker，如果没有指定 `type` 属性，默认是 `classic`。

具体参考 [Worker() - Web API 接口参考 | MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/Worker/Worker#%E5%8F%82%E6%95%B0)

打开浏览器，结果均会出现报错信息，worker1 和 worker2 的报错信息是：

```JavaScript
Uncaught SyntaxError: Unexpected token '<'
```

worker3 的报错信息是：

```JavaScript
Failed to load module script: Expected a JavaScript module script but the server responded with a MIME type of "text/html". Strict MIME type checking is enforced for module scripts per HTML spec.
```

原因是 Vue CLI 项目是使用 webpack 打包的，所以要处理 worker.js 文件，需要相关的 webpack loader 来处理的。

这里推荐的是官方的 [worker-loader](https://v4.webpack.js.org/loaders/worker-loader/)。

注意这是 webpack 4 版本的，而 webpack 5 已经内置 [Web Workers](https://webpack.js.org/guides/web-workers/) 来处理 worker.js 文件了。

一般的使用方式有两种：

- 一是通过内联的方式来引入并处理 worker.js 文件

```JavaScript
import Worker from "worker-loader!./Worker.js";
```

- 二是通过修改 webpack 配置文件，因为我们是 Vue CLI 项目，这里我们修改的是 vue.config.js 文件

```JavaScript
module.exports = {
  configureWebpack(config) {
    config.module.rules.unshift({
      test: /\.worker\.js$/, // 匹配以 .worker.js 结尾的文件
      use: { loader: 'worker-loader' }
    })
  },
}
```

上面配置的意思是往 `module.rules` 数组的最前面插入一条 `rule`。

注意，要保证在处理 js 的规则之后执行，因此上述代码不能改用 `push()`，也不要使用 `chainWebpack(config) {}`，这两种方式的结果都是往 `module.rules` 数组的最后面添加一条 `rule`，就会把处理 worker.js 文件的规则最先执行了。这会导致在某些特定操作下出现一些问题，比方如下操作：

第一次配置好，启动项目，查看页面结果没问题。然后修改 worker.js 文件，刷新页面，发现效果没有更新。则 `CTRL + C` 关闭项目，然后重启项目，页面控制台就会报错了：

```JavaScript
Uncaught SyntaxError: Unexpected token '<'
```

`configureWebpack` 和 `chainWebpack` 的具体区别参考 [配置参考 | Vue CLI](https://cli.vuejs.org/zh/config/#configurewebpack)

使用 Vue CLI 的 `vue inspect` 可以查看生成的 `rules` 情况，具体参考 [审查项目的 webpack 配置](https://cli.vuejs.org/zh/guide/webpack.html#%E5%AE%A1%E6%9F%A5%E9%A1%B9%E7%9B%AE%E7%9A%84-webpack-%E9%85%8D%E7%BD%AE)

```bash
vue inspect module.rules > rules.output.js
```

## 其他处理 Web Worker 的工具

[workerize-loader](https://github.com/developit/workerize-loader) 一个 webpack loader（与 worker-loader 类似）

[worker-plugin](https://github.com/GoogleChromeLabs/worker-plugin) 一个 webpack 插件

稍微看了以下 worker-plugin 插件的源码，其内部会拼接这么一段代码

```JavaScript
import D:\Desktop\worker-plugin-test\node_modules\worker-plugin\dist\loader.js?name=0!./workers/my.worker.js
```

其实与 worker-loader 的内联使用方式是一样的。

[simple-web-worker](https://github.com/israelss/simple-web-worker) 但是挺久没维护了，而且不支持 `Promise`

[promise-worker](https://github.com/nolanlawson/promise-worker) 以 `Promise` 的方式使用 Web Worker

## 备忘

在非 Vue CLI 项目中的 Vue 项目中，如何使用 Web Worker 并兼容 IE11，参考 [使用worker-loader在vue中使用worker，并且兼容ie11](https://blog.csdn.net/gdutRex/article/details/103860913)

```JavaScript
// 解决 window is undefined 的问题
chainWebpack(config) {
  config.output.globalObject('this')
}
```

不是所有的 npm 包都能在 Web Worker 中使用，比如 [uuid](https://github.com/uuidjs/uuid) 就不行。

## 参考资料

[vue项目中使用webWorker 时报错：Uncaught SyntaxError: Unexpected token '<'](https://www.jianshu.com/p/e5c74f82eb4a)