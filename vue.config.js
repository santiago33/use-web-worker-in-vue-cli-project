module.exports = {
  configureWebpack(config) {
    config.module.rules.unshift({
      test: /\.worker\.js$/, // 匹配以 .worker.js 结尾的文件
      use: { loader: 'worker-loader' }
    })
  }
}
